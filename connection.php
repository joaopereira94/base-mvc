<?php
class Db {
  private static $instance = NULL;

  private function __construct() {

  }


  private function __clone() {

  }

  public static function getInstance() {
    if (!isset(self::$instance)) {
      $config = parse_ini_file('config.ini');

      // $pdo = new PDO("mysql:host=servidor;dbname=banco_de_dados", 'usuario', 'senha');
      // $config['host'],$config['username'],$config['password'],$config['dbname']
      $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
      self::$instance = new PDO("mysql:host={$config['host']};dbname={$config['dbname']}", $config['username'], $config['password'], $pdo_options);
    }
    return self::$instance;
  }
}
